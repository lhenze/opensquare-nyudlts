+++
author = "Paula C Rust"
citation_link = "http://hdl.handle.net/2333.1/rfj6q71f"
date = 1995
description = "The subject of bisexuality continues to divide the lesbian and gay community. At pride marches, in films such as Go Fish, at academic conferences, the role and status of bisexuals is hotly contested. Within lesbian communities, formed to support lesbians in a patriarchal and heterosexist society, bisexual women are often perceived as a threat or as a political weakness. Bisexual women feel that they are regarded with suspicion and distrust, if not openly scorned. Drawing on her research with over 400 bisexual and lesbian women, surveying the treatment of bisexuality in the lesbian and gay press, and examining the recent growth of a self-consciously political bisexual movement, Paula Rust addresses a range of questions pertaining to the political and social relationships between lesbians and bisexual women. By tracing the roots of the controversy over bisexuality among lesbians back to the early lesbian feminist debates of the 1970s, Rust argues that those debates created the circumstances in which bisexuality became an inevitable challenge to lesbian politics. She also traces it forward, predicting the future of sexual politics."
description_html = "<p>The subject of bisexuality continues to divide the lesbian and gay community. At pride marches, in films such as Go Fish, at academic conferences, the role and status of bisexuals is hotly contested.</p> <p>Within lesbian communities, formed to support lesbians in a patriarchal and heterosexist society, bisexual women are often perceived as a threat or as a political weakness. Bisexual women feel that they are regarded with suspicion and distrust, if not openly scorned. Drawing on her research with over 400 bisexual and lesbian women, surveying the treatment of bisexuality in the lesbian and gay press, and examining the recent growth of a self-consciously political bisexual movement, Paula Rust addresses a range of questions pertaining to the political and social relationships between lesbians and bisexual women.</p> <p>By tracing the roots of the controversy over bisexuality among lesbians back to the early lesbian feminist debates of the 1970s, Rust argues that those debates created the circumstances in which bisexuality became an inevitable challenge to lesbian politics. She also traces it forward, predicting the future of sexual politics.</p>"
format = "388 pages"
isbn = 9780814774458
publisher = "New York University Press"
subjects = ["Women & Gender Studies"]
subtitle = "Sex, Loyalty, and Revolution"
thumbHref = "http://openaccessbooks.nyupress.org/oabooks-reader/epub_content/9780814774458/ops/images/9780814774458-th.jpg"
title = "Bisexuality and the Challenge to Lesbian Politics"

+++
