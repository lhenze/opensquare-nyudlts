+++
author = "Ruth Colker"
citation_link = "http://hdl.handle.net/2333.1/djh9w2k9"
date = 1996
description = "The United States, and the West in general, has always organized society along bipolar lines. We are either gay or straight, male or female, white or not, disabled or not. In recent years, however, America seems increasingly aware of those who defy such easy categorization. Yet, rather than being welcomed for the challenges that they offer, people living the gap are often ostracized by all the communities to which they might belong. Bisexuals, for instance, are often blamed for spreading AIDS to the heterosexual community and are regarded with suspicion by gays and lesbians. Interracial couples are rendered invisible through monoracial recordkeeping that confronts them at school, at work, and on official documents. In Hybrid, Ruth Colker argues that our bipolar classification system obscures a genuine understanding of the very nature of subordination. Acknowledging that categorization is crucial and unavoidable in a world of practical problems and day-to-day conflicts, Ruth Colker shows how categories can and must be improved for the good of all. "
description_html = "<p>The United States, and the West in general, has always organized society along bipolar lines. We are either gay or straight, male or female, white or not, disabled or not.</p> <p>In recent years, however, America seems increasingly aware of those who defy such easy categorization. Yet, rather than being welcomed for the challenges that they offer, people living the gap are often ostracized by all the communities to which they might belong. Bisexuals, for instance, are often blamed for spreading AIDS to the heterosexual community and are regarded with suspicion by gays and lesbians. Interracial couples are rendered invisible through monoracial recordkeeping that confronts them at school, at work, and on official documents. In <b>Hybrid</b>, Ruth Colker argues that our bipolar classification system obscures a genuine understanding of the very nature of subordination. Acknowledging that categorization is crucial and unavoidable in a world of practical problems and day-to-day conflicts, Ruth Colker shows how categories can and must be improved for the good of all. </p>"
format = "314 pages"
isbn = 9780814715383
publisher = "New York University Press"
subjects = ["Law"]
subtitle = "Bisexuals, Multiracials, and Other Misfits Under American Law"
thumbHref = "http://openaccessbooks.nyupress.org/oabooks-reader/epub_content/9780814715383/ops/images/9780814715383-th.jpg"
title = "Hybrid"

+++
