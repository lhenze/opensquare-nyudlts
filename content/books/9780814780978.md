+++
author = "Ian Shapiro, Robert Adams"
citation_link = "http://hdl.handle.net/2333.1/000001ns"
date = 1998
description = "Can individuals believe that they are acting with integrity, yet in disobedience to the dictates of their conscience? Can they retain fidelity to their conscience while ignoring a sense of what integrity requires? Integrity and conscience are often thought to be closely related, perhaps even different aspects of a single impulse. This timely book supports a different and more complicated view. Acting with integrity and obeying one's conscience might be mutually reinforcing in some settings, but in others they can live in varying degrees of mutual tension. Bringing together prominent scholars of legal theory and political philosophy, the volume addresses both classic ruminations on integrity and conscience by Plato, Hume, and Kant as well as more contemporary examinations of professional ethics and the complex relations among politics, law and personal morality."
description_html = "<p>Can individuals believe that they are acting with integrity, yet in disobedience to the dictates of their conscience? Can they retain fidelity to their conscience while ignoring a sense of what integrity requires? Integrity and conscience are often thought to be closely related, perhaps even different aspects of a single impulse. This timely book supports a different and more complicated view. Acting with integrity and obeying one's conscience might be mutually reinforcing in some settings, but in others they can live in varying degrees of mutual tension. Bringing together prominent scholars of legal theory and political philosophy, the volume addresses both classic ruminations on integrity and conscience by Plato, Hume, and Kant as well as more contemporary examinations of professional ethics and the complex relations among politics, law and personal morality.</p>"
format = "351 Pages"
isbn = 9780814780978
publisher = "New York University Press"
subjects = ["Law"]
subtitle = "Nomos XL"
thumbHref = "http://openaccessbooks.nyupress.org/oabooks-reader/epub_content/9780814780978/ops/images/9780814780978-th.jpg"
title = "Integrity and Conscience"

+++
