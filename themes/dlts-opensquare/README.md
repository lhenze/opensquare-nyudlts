# dlts-opensquare

In-progress exploration of how Hugo and its theming works

## Installation

In your Hugo site `themes` directory, clone this repo.

Next, open `config.toml` in the base of the Hugo site and ensure the theme option is set to `dlts-opensquare`.

```
theme = "dlts-opensquare"
```

For more information read the official [setup guide](//gohugo.io/overview/installing/) of Hugo.


## License

This theme is derived from https://github.com/vimux/blank and also released under the [MIT license](//github.com/Vimux/blank/blob/master/LICENSE.md).